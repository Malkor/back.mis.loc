<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceListsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('price_lists', function (Blueprint $table) {
      $table->id();
      $table->timestamps();
      $table->bigInteger('filial_id')->unsigned();
      $table->foreign('filial_id')->references('id')->on('filial_lists');
      $table->string('service_name');
      $table->text('service_body');
      $table->float('service_price');
      $table->boolean('status')->default(true);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('price_lists');
  }
}
