<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('schedules', function (Blueprint $table) {
      $table->id();
      $table->timestamps();
      $table->timestamp('date');

      $table->time('start_work_time');
      $table->time('end_work_time');

      $table->bigInteger('doctor_id')->unsigned();
      $table->foreign('doctor_id')->references('id')->on('user_lists');

      $table->bigInteger('cabinet')->unsigned();
      $table->foreign('cabinet')->references('id')->on('cabinet_lists');

      $table->bigInteger('schedules_creator')->unsigned();
      $table->foreign('schedules_creator')->references('id')->on('user_lists');

      $table->bigInteger('schedules_updater')->unsigned();
      $table->foreign('schedules_updater')->references('id')->on('user_lists');

      $table->boolean('schedules_status')->default(true);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('schedules');
  }
}
