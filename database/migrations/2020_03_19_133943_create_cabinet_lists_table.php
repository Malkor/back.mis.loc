<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCabinetListsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('cabinet_lists', function (Blueprint $table) {
      $table->id();
      $table->timestamps();
      $table->bigInteger('filial_id')->unsigned();
      $table->foreign('filial_id')->references('id')->on('filial_lists');
      $table->bigInteger('cabinet_type')->unsigned();
      $table->foreign('cabinet_type')->references('id')->on('cabinet_types');
      $table->string('cabinet_number', 10);
      $table->boolean('cabinet_status')->default(true);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('cabinet_lists');
  }
}
