<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('appointments', function (Blueprint $table) {
      $table->id();
      $table->timestamps();

      $table->timestamp('appointments_date');

      $table->bigInteger('appointments_cabinet')->unsigned();
      $table->foreign('appointments_cabinet')->references('id')->on('cabinet_lists');

      $table->bigInteger('appointments_service')->unsigned();
      $table->foreign('appointments_service')->references('id')->on('services');

      $table->bigInteger('appointments_doctor')->unsigned();
      $table->foreign('appointments_doctor')->references('id')->on('user_lists');

      $table->bigInteger('appointments_patient')->unsigned();
      $table->foreign('appointments_patient')->references('id')->on('user_lists');

      $table->bigInteger('application_creator')->unsigned();
      $table->foreign('application_creator')->references('id')->on('user_lists');

      $table->bigInteger('application_updater')->unsigned();
      $table->foreign('application_updater')->references('id')->on('user_lists');

      $table->bigInteger('appointments_status')->unsigned();
      $table->foreign('appointments_status')->references('id')->on('appointment_statuses');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('appointments');
  }
}
