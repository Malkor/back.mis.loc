<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_lists', function (Blueprint $table) {
            $table->id();

            $table->string('first_name');
            $table->string('name');
            $table->string('last_name');

            $table->string('password');
            $table->string('email')->unique();

            $table->bigInteger('role_id')->unsigned()->default(0);
            $table->bigInteger('filial_id')->unsigned();
            $table->bigInteger('service_id')->unsigned()->default(0);
            $table->boolean('active')->default(true);




            $table->foreign('role_id')->references('id')->on('role_lists');
            $table->foreign('filial_id')->references('id')->on('filial_lists');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_lists');
    }
}
