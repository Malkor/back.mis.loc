<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('/cabinets/list')->group(function () {
  Route::get('', 'Admin\Info\CabinetListController@index');
  Route::post('create', 'Admin\Info\CabinetListController@store');
  Route::get('show/{id}', 'Admin\Info\CabinetListController@show')->where('id', '[0-9]+');
  Route::post('update', 'Admin\Info\CabinetListController@update')->where('id', '[0-9]+');
  Route::get('destroy/{id}', 'Admin\Info\CabinetListController@destroy')->where('id', '[0-9]+');
});



Route::group([
    'prefix' => ''
], function () {
    Route::post('login', 'Admin\User\UserController@login');
    Route::post('signup', 'Admin\User\UserController@signup');
    #Route::post('forgot', 'Admin\User\UserController@forgot');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'Admin\User\UserController@logout');
        Route::get('user', 'Admin\User\UserController@user');
    });
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'password'
], function () {
    Route::post('create', 'Admin\User\PasswordResetController@create');
    Route::get('find/{token}', 'Admin\User\PasswordResetController@find');
    Route::post('reset', 'Admin\User\PasswordResetController@reset');
});

Route::prefix('/cabinets/types')->group(function () {
  Route::get('', 'Admin\Info\CabinetTypeController@index');
  Route::post('create', 'Admin\Info\CabinetTypeController@store');
  Route::get('show/{id}', 'Admin\Info\CabinetTypeController@show')->where('id', '[0-9]+');
  Route::post('update', 'Admin\Info\CabinetTypeController@update')->where('id', '[0-9]+');
});

Route::prefix('/price/list')->group(function () {
  Route::get('', 'Admin\Info\PriceListController@index');
  Route::post('create', 'Admin\Info\PriceListController@store');
  Route::get('show/{id}', 'Admin\Info\PriceListController@show')->where('id', '[0-9]+');
  Route::post('update', 'Admin\Info\PriceListController@update');
  Route::get('destroy/{id}', 'Admin\Info\PriceListController@destroy')->where('id', '[0-9]+');
});

Route::prefix('/filial/list')->group(function () {
  Route::get('', 'Admin\Info\FilialController@index');
  Route::post('create', 'Admin\Info\FilialController@store');
  Route::post('update', 'Admin\Info\FilialController@update');
  Route::get('show/{id}', 'Admin\Info\FilialController@show')->where('id', '[0-9]+');
  Route::get('destroy/{id}', 'Admin\Info\FilialController@destroy')->where('id', '[0-9]+');
});
