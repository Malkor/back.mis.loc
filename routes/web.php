<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/', function () {
    return view('welcome');
});


//admin route for companies and filials
$groupInfoData = [
  'namespace' => 'Admin\Info',
  'prefix' => 'admin/info'
];

Route::group($groupInfoData, function(){
  $method = ['index', 'edit', 'store', 'update', 'create'];
  Route::resource('filials', 'FilialController')
          -> only($method)
          -> names('admin.info.filials');

  Route::resource('company', 'CompanyController')
          -> only($method)
          -> names('admin.info.company');
});

//main panel with menu and other shit
$adminData = [
  'namespace' => 'Admin',
  'prefix' => 'admin/'
];

Route::group($adminData, function(){
  Route::resource('', 'AdminController')
          -> only('index')
          -> names('admin');
});



/*
//register routes
Route::get('/admin/register', 'Admin\RegistrationController@create');
Route::post('/admin/register', 'Admin\RegistrationController@store');

Route::get('/admin/', 'Admin\SessionsController@create');
Route::post('/admin/', 'Admin\SessionsController@store');
Route::get('/admin/logout', 'Admin\SessionsController@destroy');*/
