<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;

abstract class BaseRESTController extends Controller
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  public function errorResponse($params, $status = false)
  {
    // Если хотим указать статус ошибки, то указываем любой кроме 200
    //По дефолту 418 HTTP_I_AM_A_TEAPOT
    $status ?: $status = '418';
    return new JsonResponse([
      'status' => $status,
      'errors' => $params
    ], JsonResponse::HTTP_OK);
  }

  public function successResponse($params)
  {
    // если всё ок возвращаем статус 200
    return new JsonResponse([
      'status' => '200',
      'result' => $params
    ], JsonResponse::HTTP_OK);

  }
}