<?php

namespace App\Http\Controllers\Admin\Info;

use App\Http\Controllers\Admin\BaseRESTController;
use App\Models\Appointment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * @method getUser()
 */
class AppointmentController extends BaseRESTController
{
  /**
   * Display a listing of the resource.
   *
   * @return JsonResponse
   */
  public function index()
  {
    return $this->successResponse(Appointment::all());
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function store(Request $request)
  {
    //TODO Описать поля для обновления
    $this->getUser();
    $new_item = new Appointment($request->all());
    $new_item->save();

    return $this->successResponse($new_item);
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return JsonResponse
   */
  public function show($id)
  {
    return $this->successResponse(Appointment::findOrFail($id));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function update(Request $request)
  {

    $item = Appointment::find($request->get('id'));

    ///TODO Описать поля для обновления
    $item->save();
    return $this->successResponse($item);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return JsonResponse
   */
  public function destroy($id)
  {
//    $item = Appointment::findOrFail($id);
//    $item->save();
    ///TODO Внести изменения в алгоритм выключеня элемента расписания. ( Перенос элементов в стэк )
    return $this->successResponse('Appointment destroy');
  }
}
