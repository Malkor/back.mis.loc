<?php

namespace App\Http\Controllers\Admin\Info;

use App\Http\Controllers\Admin\BaseRESTController;
use App\Http\Requests\AppointmentStatusRequest;
use App\Http\Requests\AppointmentStatusUpdateRequest;
use App\Models\AppointmentStatus;
use Illuminate\Http\JsonResponse;

class AppointmentStatusController extends BaseRESTController
{
  /**
   * Display a listing of the resource.
   *
   * @return JsonResponse
   */
  public function index()
  {
    return $this->successResponse(AppointmentStatus::all());
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param AppointmentStatusRequest $request
   * @return JsonResponse
   */
  public function store(AppointmentStatusRequest $request)
  {
    $new_item = new AppointmentStatus($request->all());
    $new_item->save();
    return $this->successResponse($new_item);
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return JsonResponse
   */
  public function show($id)
  {
    return $this->successResponse(AppointmentStatus::findOrFail($id));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param AppointmentStatusUpdateRequest $request
   * @return JsonResponse
   */
  public function update(AppointmentStatusUpdateRequest $request)
  {
    $item = AppointmentStatus::find($request->get('id'));
    $item->appointment_statuses_name = $request->get('appointment_statuses_name');
    $item->save();
    return $this->successResponse($item);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return JsonResponse
   */
  public function destroy($id)
  {
    $item = AppointmentStatus::findOrFail($id);
    $item->appointment_statuses_status = false;
    $item->save();
    return $this->successResponse('Appointment Status destroy');
  }
}
