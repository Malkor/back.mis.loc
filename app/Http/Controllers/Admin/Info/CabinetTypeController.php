<?php

namespace App\Http\Controllers\Admin\Info;

use App\Http\Controllers\Admin\BaseRESTController;
use App\Http\Requests\CabinetTypeRequest;
use App\Http\Requests\CabinetTypeUpdateRequest;
use App\Models\CabinetType;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CabinetTypeController extends BaseRESTController
{
  /**
   * Display a listing of the resource.
   *
   * @return JsonResponse
   */
  public function index()
  {
    return $this->successResponse(CabinetType::all());
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param CabinetTypeRequest $request
   * @return JsonResponse
   */
  public function store(CabinetTypeRequest $request)
  {
    $new_cabinet = new CabinetType($request->all());
    $new_cabinet->save();

    return $this->successResponse($new_cabinet);
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return JsonResponse
   */
  public function show($id)
  {
    return $this->successResponse(CabinetType::find($id));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param CabinetTypeUpdateRequest $request
   * @return JsonResponse
   */
  public function update(CabinetTypeUpdateRequest $request)
  {
    $item = CabinetType::find($request->input('id'));
    $item->cabinet_type_name = $request->input('cabinet_type_name');
    $item->save();

    return $this->successResponse($item);
  }


}
