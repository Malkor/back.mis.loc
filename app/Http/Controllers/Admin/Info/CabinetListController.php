<?php

namespace App\Http\Controllers\Admin\Info;

use App\Http\Controllers\Admin\BaseRESTController;
use App\Http\Requests\CabinetListRequest;
use App\Http\Requests\CabinetListUpdateRequest;
use App\Models\CabinetList;
use Illuminate\Http\JsonResponse;

class CabinetListController extends BaseRESTController
{

  /**
   * Display a listing of the resource.
   *
   * @return JsonResponse
   */
  public function index()
  {
    return $this->successResponse(CabinetList::all());
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param CabinetListRequest $request
   * @return JsonResponse
   */
  public function store(CabinetListRequest $request)
  {
    $new_cabinet = new CabinetList($request->all());
    $new_cabinet->save();

    return $this->successResponse($new_cabinet);
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return JsonResponse
   */
  public function show($id)
  {
    return $this->successResponse(CabinetList::findOrFail($id));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param CabinetListUpdateRequest $request
   * @return JsonResponse
   */
  public function update(CabinetListUpdateRequest $request)
  {
    $item = CabinetList::find($request->input('id'));

    is_null($request->get('filial_id')) ?: $item->filial_id = $request->get('filial_id');
    is_null($request->get('cabinet_number')) ?: $item->cabinet_number = $request->get('cabinet_number');
    is_null($request->get('cabinet_type')) ?: $item->cabinet_type = $request->get('cabinet_type');

    $item->save();

    return $this->successResponse($item);
  }


  /**
   * @param $id
   * @return JsonResponse
   */
  public function destroy($id)
  {
    $emp = CabinetList::find($id);
    $emp->cabinet_status = false;
    $emp->save();

    return $this->successResponse('destroy cabinet');
  }


}
