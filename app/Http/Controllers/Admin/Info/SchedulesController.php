<?php

namespace App\Http\Controllers\Admin\Info;

use App\Http\Controllers\Admin\BaseRESTController;
use App\Http\Requests\SchedulesRequest;
use App\Http\Requests\SchedulesUpdateRequest;
use App\Models\Schedule;
use Illuminate\Http\JsonResponse;

class SchedulesController extends BaseRESTController
{
  ///TODO need testing ! update and create methods
  /**
   * @return JsonResponse
   */
  public function index()
  {
    return $this->successResponse(Schedule::all());
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param SchedulesRequest $request
   * @return JsonResponse
   */
  public function store(SchedulesRequest $request)
  {
    $new_item = new Schedule($request->all());
    $new_item->save();
    return $this->successResponse($new_item);
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return JsonResponse
   */
  public function show($id)
  {
    return $this->successResponse(Schedule::findOrFail($id));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param SchedulesUpdateRequest $request
   * @return JsonResponse
   */
  public function update(SchedulesUpdateRequest $request)
  {
    $item = Schedule::find($request->get('id'));

    is_null($request->get('start_work_time')) ?: $item->start_work_time = $request->get('start_work_time');
    is_null($request->get('end_work_time')) ?: $item->end_work_time = $request->get('end_work_time');
    is_null($request->get('doctor_id')) ?: $item->doctor_id = $request->get('doctor_id');
    is_null($request->get('cabinet')) ?: $item->cabinet = $request->get('cabinet');
    is_null($request->get('schedules_creator')) ?: $item->schedules_creator = $request->get('schedules_creator');
    is_null($request->get('schedules_updater')) ?: $item->schedules_updater = $request->get('schedules_updater');

    $item->save();
    return $this->successResponse('asdasd');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return JsonResponse
   */
  public function destroy($id)
  {
    $item = Schedule::findOrFail($id);
    $item->schedules_status = false;
    $item->save();

    return $this->successResponse('schedules destroy');
  }
}
