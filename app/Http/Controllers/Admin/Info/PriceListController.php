<?php

namespace App\Http\Controllers\Admin\Info;

use App\Http\Controllers\Admin\BaseRESTController;
use App\Http\Requests\PriceListRequest;
use App\Http\Requests\PriceListUpdateRequest;
use App\Models\PriceList;
use Illuminate\Http\JsonResponse;

class PriceListController extends BaseRESTController
{
  /**
   * Display a listing of the resource.
   *
   * @return JsonResponse
   */
  public function index()
  {
    return $this->successResponse(PriceList::all());
  }

  /**
   * @param PriceListRequest $request
   * @return JsonResponse
   */
  public function store(PriceListRequest $request)
  {
    $item = new PriceList($request->all());
    $item->save();
    return $this->successResponse($item);
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return JsonResponse
   */
  public function show($id)
  {
    return $this->successResponse(PriceList::findOrFail($id));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param PriceListUpdateRequest $request
   * @return JsonResponse
   */
  public function update(PriceListUpdateRequest $request)
  {
    $item = PriceList::find($request->get('id'));

    is_null($request->get('filial_id')) ?: $item->filial_id = $request->get('filial_id');
    is_null($request->get('service_name')) ?: $item->service_name = $request->get('service_name');
    is_null($request->get('service_body')) ?: $item->service_body = $request->get('service_body');
    is_null($request->get('service_price')) ?: $item->service_price = $request->get('service_price');

    $item->save();
    return $this->successResponse($item);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return JsonResponse
   */
  public function destroy($id)
  {
    $emp = PriceList::find($id);
    $emp->status = false;
    $emp->save();

    return $this->successResponse('destroy');
  }
}
