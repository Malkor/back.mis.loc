<?php

namespace App\Http\Controllers\Admin\Info;

use App\Http\Controllers\Admin\BaseRESTController;
use App\Http\Requests\FilialRequest;
use App\Http\Requests\FilialUpdateRequest;
use App\Models\FilialList;
use Illuminate\Http\JsonResponse;

class FilialController extends BaseRESTController
{

  /**
   * @return JsonResponse
   */
  public function index()
  {
    return $this->successResponse(FilialList::all());
  }

  /**
   * @param FilialRequest $request
   * @return JsonResponse
   */
  public function store(FilialRequest $request)
  {
    $item = new FilialList($request->input());
    $item->save();

    return $this->successResponse($item);
  }

  /**
   * @param $id
   * @return JsonResponse
   */
  public function show($id)
  {
    return $this->successResponse(FilialList::findOrFail($id));
  }


  /**
   * @param FilialUpdateRequest $request
   * @return JsonResponse
   */
  public function update(FilialUpdateRequest $request)
  {
    $item = FilialList::find($request->input('id'));

    is_null($request->get('company_id')) ?: $item->company_id = $request->get('company_id');
    is_null($request->get('name')) ?: $item->name = $request->get('name');
    is_null($request->get('description')) ?: $item->description = $request->get('description');

    $item->save();

    return $this->successResponse($item);
  }

  /**
   * @param $id
   * @return JsonResponse
   */
  public function destroy($id)
  {
    $emp = FilialList::find($id);
    $emp->status = false;
    $emp->save();

    return $this->successResponse('destroy filial');
  }
}
