<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PriceListUpdateRequest extends BaseRESTFormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'id' => 'required|integer|exists:price_lists,id',
      'filial_id' => 'integer|exists:filial_lists,id',
      'service_name' => 'string|max:225',
      'service_body' => 'string',
      'service_price' => 'numeric',
    ];
  }
}
