<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppointmentRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'appointments_date' => 'required|integer',
      'appointments_cabinet' => 'required|integer|exists:cabinet_lists,id',
      'appointments_service' => 'required|integer|exists:services,id',
      'appointments_doctor' => 'required|integer|exists:user_lists,id',
      'appointments_patient' => 'required|integer|exists:user_lists,id',
    ];
  }
}
