<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SchedulesRequest extends BaseRESTFormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'start_work_time' => 'required|integer',
      'end_work_time' => 'required|integer',
      'doctor_id' => 'required|integer|exists:user_lists,id',
      'cabinet' => 'required|integer|exists:cabinet_lists,id',
      'schedules_creator' => 'required|integer|exists:user_lists,id',
      'schedules_updater' => 'required|integer|exists:user_lists,id'
    ];
  }
}
