<?php

namespace App\Http\Requests;

class CabinetListRequest extends BaseRESTFormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'filial_id' => 'required|integer|exists:filial_lists,id',
      'cabinet_type' => 'required|integer|exists:cabinet_types,id',
      'cabinet_number' => 'required|integer',
    ];
  }

}
