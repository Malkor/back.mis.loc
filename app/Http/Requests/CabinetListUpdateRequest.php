<?php

namespace App\Http\Requests;

class CabinetListUpdateRequest extends BaseRESTFormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'id' => 'required|integer|exists:cabinet_lists,id',
      'filial_id' => 'integer|exists:filial_lists,id',
      'cabinet_type' => 'integer|exists:cabinet_types,id',
      'cabinet_number' => 'integer',
    ];
  }
}
