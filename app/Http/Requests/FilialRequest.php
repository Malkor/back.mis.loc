<?php

namespace App\Http\Requests;

class FilialRequest extends BaseRESTFormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */

  public function rules()
  {
    return [
      'name' => 'required|string|min:5|max:150',
      'description' => 'required|string|max:500|min:20',
      'company_id' => 'required|integer|exists:company_lists,id'
    ];
  }
}