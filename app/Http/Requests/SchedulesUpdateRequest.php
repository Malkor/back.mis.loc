<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SchedulesUpdateRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'id' => 'required|integer|exists:schedules,id',
      'start_work_time' => 'integer',
      'end_work_time' => 'integer',
      'doctor_id' => 'integer|exists:user_lists,id',
      'cabinet' => 'integer|exists:cabinet_lists,id',
      'schedules_creator' => 'integer|exists:user_lists,id',
      'schedules_updater' => 'integer|exists:user_lists,id'
    ];
  }
}
