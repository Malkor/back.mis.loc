<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilialUpdateRequest extends BaseRESTFormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'id' => 'required|integer|exists:filial_lists,id',
      'company_id' => 'integer|exists:company_lists,id',
      'name' => 'string|min:5|max:150',
      'description' => 'string|min:20|max:500'
    ];
  }
}
