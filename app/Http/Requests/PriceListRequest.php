<?php

namespace App\Http\Requests;

use App\Http\Controllers\Admin\BaseRESTController;

class PriceListRequest extends BaseRESTFormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'filial_id' => 'required|integer|exists:filial_lists,id',
      'service_name' => 'required|string|max:225',
      'service_body' => 'required|string',
      'service_price' => 'required|numeric',
    ];
  }

}
