<?php


namespace App\Http;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

interface RestControllerInterface
{
  public function create($request = null);

  public function read();

  public function update(Request $request);

  public function delete(Request $request);

}
