<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static find(int $id)
 * @method static findOrFail(int $id)
 */
class CabinetList extends Model
{
  protected $fillable = [
    'cabinet_number',
    'cabinet_type',
    'cabinet_status',
    'filial_id'
  ];
}
