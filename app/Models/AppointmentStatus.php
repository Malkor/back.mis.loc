<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * @method static find(int $id)
 * @method static findOrFail(int $id)
 */
class AppointmentStatus extends Model
{
  protected $fillable = [
    'appointment_statuses_name',
    'appointment_statuses_status'
  ];
}
