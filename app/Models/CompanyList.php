<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyList extends Model
{
    public function filialList()
    {
      return $this->hasMany(FilialList::class);
    }
}
