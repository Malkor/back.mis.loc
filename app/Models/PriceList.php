<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static findOrFail(int $id)
 * @method static find(int $id)
 */
class PriceList extends Model
{
  protected $fillable = [
    'filial_id',
    'service_name',
    'service_body',
    'service_price',
    'status'
  ];
}
