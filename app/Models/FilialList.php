<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static findOrFail($id)
 * @method static find($input)
 */
class FilialList extends Model
{
  protected $fillable = [
    'name',
    'description',
    'company_id',
    'status'
  ];

  public function companyData()
  {
    return $this->belongsTo('CompanyList', 'foreign_key');
  }

}
