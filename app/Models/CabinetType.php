<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static find(int $id)
 */
class CabinetType extends Model
{
  protected $fillable = [
    'cabinet_type_name'
  ];
}
