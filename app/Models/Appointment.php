<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static findOrFail(int $id)
 * @method static find($get)
 */
class Appointment extends Model
{
  protected $fillable = [
    'appointments_date',
    'appointments_cabinet',
    'appointments_service',
    'appointments_doctor',
    'appointments_patient',
    'application_creator',
    'application_updater',
    'appointments_status',
  ];
}
