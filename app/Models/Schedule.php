<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * @method static find(int $id)
 * @method static findOrFail(int $id)
 */
class Schedule extends Model
{
  protected $fillable = [
    'start_work_time',
    'end_work_time',
    'doctor_id',
    'cabinet',
    'schedules_creator',
    'schedules_updater',
    'schedules_status'
  ];
}
