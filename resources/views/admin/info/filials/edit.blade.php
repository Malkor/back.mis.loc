

<form class="" action="{{ route('admin.info.filials.update', $filial->id) }}" method="post">
  @method('PATCH')
  @csrf
  <label for="">Name</label><br>
  <input type="text" name="name" value="{{ $filial->name }}"><br>
  <label for="">Description</label><br>
  <textarea name="description" rows="8" cols="80">{{ $filial->description }}</textarea><br>
  <button type="submit" name="button">Сохранить</button>
</form>
<a href="{{ route('admin.info.filials.index') }}">Вернуться к списку</a>
