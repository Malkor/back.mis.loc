

<form class="" action="{{ route('admin.info.filials.store') }}" method="post">
  @method('POST')
  @csrf
  <input type="hidden" name="company_id" value="1">
  <label for="">Название</label><br>
  <input type="text" name="name" value="{{ $filial->name }}"><br>
  <label for="">Описание</label><br>
  <textarea name="description" rows="8" cols="80">{{ $filial->description }}</textarea><br>

  <button type="submit" name="button">Сохранить</button>
</form>
<a href="{{ route('admin.info.filials.index') }}">Отменить</a>
